# Use openjdk image
FROM openjdk:19-jdk-alpine3.16

# Settings up build argument
ARG PROFILE=mysql

# Storing the git commit hash and date as build argument
ARG GITHASH

# Setting up environment variables for spring
ENV SPRING_PROFILES_ACTIVE=$PROFILE

# Passing the value of the build argument to an environment variable
# This is because build arguments are not available once the container is built
ENV HASH=$GITHASH

# Create a directory to save the jar in
RUN mkdir -p /home/app

# Create a non root user
RUN adduser -D assignment

# Copy the necessary files from the target into the image
COPY ./target/assignment-$GITHASH.jar /home/app/assignment-$GITHASH.jar

# Grant user assignment recursive ownership of the newly created directory and its contents
RUN chown -R assignment:assignment /home/app/

# Changing into the user assignment
USER assignment

# Set the working directory
WORKDIR /home/app

# Expose application port
EXPOSE 8090

# Run the java application
# Note that I am using shell form instead of exec form for the CMD instruction
# This is an explicit choice as variable expansion does not occur with the exec
# form of CMD instruction and the value of ${HASH} is not substituted
CMD java -jar assignment-${HASH}.jar
