1. What does JVM mean?

JVM is a part of the Java Runtime Environment. It stands for Java Virtual Machine and provides the runtime environment that allows us to run Java bytecodes. It performs just-in-time compilation of Java bytecode to machine bytecode and performs execution of the Java program.

2. What is application server?

An application server is a program that facilitates communication between application clients and server code. Application servers work differently than a web server in that they can execute code to serve content dynamically. It hosts applications and allows access to the application's interface over different communication protocols.

3. What are WAR files and how does tomcat deal with them?

WAR files stand for web application archive. They are a file format used to distribute collections of JAR files, and other web application resources.

Tomcat creates class loaders for all deployed WARs and performs resource allocation and management.

There are several ways to deploy web applications using tomcat. We can use the manager webapp to specify the path of the webapp inside the machine and deploy it. We can also upload the .war file using the manager webapp and deploy the webapp. Finally, we can also deploy the web app by placing the contents of the web app inside the $CATALINA\_HOME/webapps directory.

4. Deploy an application with Tomcat.

Firstly, we must install tomcat. To do so, we can download the tarball from apache website. After extracting the downloaded tarball, we must create a tomcat user and recursively grant ownership of the extracted tomcat directory and it's contents to the newly created tomcat user using `chown -R tomcat:tomcat [[extracted directory]]`. The files needed to start and stop tomcat are stored in the bin directory of the extracted archive. To start the tomcat server, we need to run the startup.sh shell script.

Tomcat by default runs on port 8080. There are two management consoles installed by default. The manager and host manager web app. To access these, we must first add the requisite users to the tomcat-users.xml file.

```
<role rolename="admin-gui"/>
<role rolename="manager-gui"/>
<user username="manager" password="manager" roles="manager-gui"/>
<user username="admin" password="admin" roles="admin-gui"/>
```

The manager and host manager applications use basic http authentication. The manager-gui role lets users access the manager web app while the admin-gui role lets users access the host-manager webapp. Once we add these roles, we must restart the tomcat server if it is already running by running the shutdown.sh shell script and then startup.sh shell script.

Once we log into the manager webapp, we can upload the .war file into the manager webapp and then deploy the newly uploaded webapp right from the manager webapp.

5. Is running multiple instances of Tomcat beneficial?

Running multiple instances of tomcat can be benefecial under some circumstances. It can be useful for testing purposes and for isolating different web applications running on tomcat.

6. Configure two web applications; one accessible pulicly and the other from the localhost.

To control access to a web application, we need to specify what remote addresses the application is reachable from by modifying the contents of the context.xml file for each specific webapp we wish to do this modification on.

The following web app would only be available on the localhost.
```
<?xml version="1.0" encoding="UTF-8"?>
<Context>
  <CookieProcessor className="org.apache.tomcat.util.http.Rfc6265CookieProcessor"
                   sameSiteCookies="strict" />
  <Valve className="org.apache.catalina.valves.RemoteAddrValve"
         allow="127\.\d+\.\d+\.\d+|::1|0:0:0:0:0:0:0:1" />
</Context>
```

The following web app would be available on the localhost and wider local network with addresses matching 192.168.\*.\*.
```
<?xml version="1.0" encoding="UTF-8"?>
<Context>
  <CookieProcessor className="org.apache.tomcat.util.http.Rfc6265CookieProcessor"
                   sameSiteCookies="strict" />
  <Valve className="org.apache.catalina.valves.RemoteAddrValve"
         allow="192\.168\.\d+\.\d+|127\.\d+\.\d+\.\d+|::1|0:0:0:0:0:0:0:1" />
</Context>
```

7. Configure two instance of Tomcat; one accessible over HTTPS and the other over HTTP.

To run multiple instances of tomcat on the same server, we need to create multiple users who each have tomcat installed locally.

Once the users are created and each user has tomcat installed locally, we can create systemd service files to make startup of both tomcat instances easier.

To use HTTPS with tomcat, we need to create a self-signed certificate. The easiest way to do this for tomcat is to use the `keytool` application. Using `$ keytool -genkey -keyalg RSA -alias tomcat -keystore tomcat.jks -keysize 2048 -validity 365`, we can generate a 2048 bit RSA signing certificate with a validity of one year aliased to tomcat.

Once we have the certificate, we need to configure tomcat to use this certificate. To do so, we need to make modifications to the server.xml file inside $CATALINA\_HOME/conf. To make tomcat use the generated certificate, we have to add the following lines to server.xml.

```
    <Connector port="8082" protocol="HTTP/1.1" SSLEnabled="true"
            maxThreads="150" scheme="https" secure="true"
            clientAuth="false" sslProtocol="TLS"
            keystoreFile="/path/to/tomcat.jks"
            keystorePass="tomcat"
            keyAlias="tomcat" />
```

Once we have made the modification to server.xml, we can restart the tomcat server using `systemctl restart` if we have written the systemd service file or by running the shutdown.sh and startup.sh shell scripts. The tomcat instance will now be reachable over https on port 8082.
