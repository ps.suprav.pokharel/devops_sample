### Note

If using the `git-commit-id-plugin`, the file name of the compiled `.jar` file is changed to the commit date and commit hash but the version listed in java manifest is static as declared in the `pom.xml`. If we want to change the version inside the `.pom.xml` before compilation, we can use the following one-liner before packaging.

`mvn org.codehaus.mojo:versions-maven-plugin:set -DnewVersion=$(git show --no-patch --no-notes --abbrev=8 --pretty='%cs' | sed 's/^20//g' | sed -E 's/([0-9])-([0-9])/\1\2/g')-$(git show --no-patch --no-notes --abbrev=8 --pretty='%h')`
