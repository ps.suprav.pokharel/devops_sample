### Insert new data

`curl -X POST 127.0.0.1:8090/persons/ -d '{"name":"test","age":"100"}' -H "Content-Type:application/json"`

`curl -X POST 127.0.0.1:8090/persons/ -d '{"name":"testing","age":"100"}' -H "Content-Type:application/json"`

`curl -X POST 127.0.0.1:8090/persons/ -d '{"name":"tester","age":"100"}' -H "Content-Type:application/json"`

### Update record of a specific person

`curl -X PUT 127.0.0.1:8090/persons/1 -d '{"name":"tests","age":"100"}' -H "Content-Type:application/json"`

### Delete records of a specific person

`curl -X DELETE 127.0.0.1:8090/persons/2`

### Show records

`curl -X GET 127.0.0.1:8090/persons/`

### Show record of a specific person

`curl -X GET 127.0.0.1:8090/persons/1`
